import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div className='bg-dark text-center text-white p-4'style={{border: '2px solid blue'}}>Copyright © Your Website 2022</div>
    )
  }
}
