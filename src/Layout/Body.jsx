import React, { Component } from "react";

export default class Body extends Component {
  render() {
    return (
      <div style={{ border: '2px solid red' }}>
        <div className="welcome w-75 mx-auto p-4 my-3 bg-light" style={{ border: '2px solid blue' }}>
          <h1 className="display-4">A Warm Welcome!</h1>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Error
            necessitatibus eum ad, voluptates officiis harum quo at, veritatis
            ea aspernatur ab dignissimos nobis, aperiam porro!
          </p>
          <button type="button" className="btn btn-primary">
            Call to action!
          </button>
        </div>
        <div className="carousel w-75 mx-auto">
          <div className="card-deck mb-3">
            <div className="card text-center" style={{ border: '2px solid purple' }}>
              <img className="card-img-top" style={{ height: "12rem" }}
                src="https://picsum.photos/seed/picsum/200/300" alt="Card image cap" />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum obcaecati mollitia cum!</p>
              </div>
              <div className="card-footer">
                <a href="#" className="btn btn-primary">
                  Find Out More!
                </a>
              </div>
            </div>
            <div className="card text-center" style={{ border: '2px solid purple' }}>
              <img className="card-img-top" style={{ height: "12rem" }}
                src="https://picsum.photos/seed/picsum/200/300" alt="Card image cap" />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur deserunt explicabo minima placeat voluptatum, esse suscipit nihil.</p>
              </div>
              <div className="card-footer">
                <a href="#" className="btn btn-primary">
                  Find Out More!
                </a>
              </div>
            </div>
            <div className="card text-center" style={{ border: '2px solid purple' }}>
              <img className="card-img-top" style={{ height: "12rem" }}
                src="https://picsum.photos/seed/picsum/200/300" alt="Card image cap" />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur tenetur corrupti voluptate.</p>
              </div>
              <div className="card-footer">
                <a href="#" className="btn btn-primary">
                  Find Out More!
                </a>
              </div>
            </div>
            <div className="card text-center" style={{ border: '2px solid purple' }}>
              <img className="card-img-top" style={{ height: "12rem" }}
                src="https://picsum.photos/seed/picsum/200/300" alt="Card image cap" />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Rerum debitis neque ex est voluptates accusantium nam natus.</p>
              </div>
              <div className="card-footer">
                <a href="#" className="btn btn-primary">
                  Find Out More!
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
